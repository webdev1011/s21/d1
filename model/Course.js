const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Course name']
	},
	description: {
		type: String,
		required: [true, 'Course Description']
	},
	price: {
		type: Number,
		required: [true, 'Course price']
	},
	isActive: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [{
			userId: {
				type: String,
				required: [true, 'Use ID required']
			},
			enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
	]
	
})

module.exports = mongoose.model('course', courseSchema);