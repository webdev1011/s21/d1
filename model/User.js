const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is requied.']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is requied.']
	},
	email: {
		type: String,
		required: [true, 'Email name is requied.']
	},
	password: {
		type: String,
		required: [true, 'Password name is requied.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'MObile Number name is requied.']
	},
	enrollments: [
		{
			courseId: {
				type: String,
				requied: [true, 'Course ID is requied.']
		
		},
			enrolledOn:{
				type: Date,
				default: new Date()
		},
			status: {
				type: String,
				default: 'enrolled'
			}
	}

  ]
  
});


module.exports = mongoose.model('User', userSchema);