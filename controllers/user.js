const User = require('../model/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth')
const Course = require('../model/Course.js')

module.exports.emailExist = (params) => {
	return User.find ( { email: params.email })
	.then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User ({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}
module.exports.enroll = () => {
	return User.findbyId(params.userId).then (user =>{
		user.enrollments.push({courseId: params.courseId})
	
	return user.save().then((user, err)=>{
		return Course.findbyId(params.courseId)
		.then(course =>{
			course.enrollees.push({userId: params.userId })

	return course.save().then ((course, err)=> {
		return (err) ? false : true

	})
	})
    })
    })
	
	}

//misc
module.exports.updateDetails = (params) => {

}

module.exports.changePassword = (params) => {
	
}

//require the authentication logic for logging in a user
module.exports.login = (params) => {
	//check email in the dbase
	return User.findOne( { email: params.email})
	.then( user => {
		if (user == null) {
			return false
		}
//compare password received and hashed password
//return true if values match
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		//the mongoose toObject method will convert the mongoose into plain javascript object
		//used to show an object representation of mongoose mode
		//mongoose object will have access to .save() method while plain javascript wont
		if (isPasswordMatched) {
			return {accesToken: auth.createAccessToken(user.toObject())}
		} else {
			return false
		}
	})
}


//create the controller logic for getting the user information
module.exports.get  = (params) => {
	return User.findById(params.userId).then(user =>{
		//re-assign password to undefined so it won't be retrieve along with other user data
		user.password = undefined
		return user
	})
}
